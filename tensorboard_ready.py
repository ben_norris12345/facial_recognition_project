"""
Author: ben norris - git push file.
Created: 2020-12-06

"""

# %%
# ------------------------------------------------------------------------------
# File I/O
# ------------------------------------------------------------------------------
DATA_DIR = r'C:\Users\JackRodwell\OneDrive - Kubrick Group\Kubrick_Python_Training\MLE01_notes\Deep Learning\Project\facial_recognition_project\data_faces'
#DATA_DIR = r'C:\Users\BenNorris\Documents\Kubrick Week 12 - Deep Learning\transfer_learning_project\DeepLearning\data'
tensorboard_dir = r'C:\Users\JackRodwell\OneDrive - Kubrick Group\Kubrick_Python_Training\MLE01_notes\Deep Learning\Project\facial_recognition_project\tensorboard_logs'

# %%
# ------------------------------------------------------------------------------
# Imports
# ------------------------------------------------------------------------------
import torch
import torch.nn as nn
import torch.optim as optim
import os
import torchvision
import torchvision.models as models
import torchvision.transforms as transforms
from torch.utils.tensorboard import SummaryWriter

import datetime as dt

import matplotlib.pyplot as plt
import numpy as np
import pandas as pd

from ignite.engine import Events, create_supervised_trainer, create_supervised_evaluator
from ignite.metrics import Loss, Accuracy

# %%
# ------------------------------------------------------------------------------
# Device Setup
# ------------------------------------------------------------------------------
device = torch.device('cuda:0' if torch.cuda.is_available() else 'cpu')
print('GPU Detected: ', torch.cuda.get_device_name())

# %%
# ------------------------------------------------------------------------------
# Tensorboard Setup
# ------------------------------------------------------------------------------

run_name = 'tensorboard_tutorial_' + dt.datetime.now().strftime('%Y-%m-%d_%H%M')
log_dir  = os.path.join(tensorboard_dir, run_name)
writer   = SummaryWriter(log_dir)

# %%
# ------------------------------------------------------------------------------
# Data Setup
# ------------------------------------------------------------------------------
IMGNET_SIZE = 224
IMGNET_MEAN = [0.485, 0.456, 0.406]
IMGNET_STD  = [0.229, 0.224, 0.225]


# Setting up the transforms
transform = transforms.Compose([transforms.RandomHorizontalFlip(),
                                transforms.Resize((IMGNET_SIZE, IMGNET_SIZE)),
                                transforms.ToTensor(),
                                transforms.Normalize(IMGNET_MEAN, IMGNET_STD)])

dataset = torchvision.datasets.ImageFolder(DATA_DIR, transform=transform)

# Splitting the dataset into train and test
dataset_size = len(dataset)
train_size   = int(dataset_size * 0.8)
test_size    = dataset_size - train_size
trainset, testset = torch.utils.data.random_split(dataset, [train_size, test_size])

# Setting up dataloaders
trainloader = torch.utils.data.DataLoader(trainset, batch_size=32, shuffle=True)
testloader  = torch.utils.data.DataLoader(testset,  batch_size=32, shuffle=True)

# Names of classes
classes = dataset.classes
print('Classes detected: ', classes)


# %%
# ------------------------------------------------------------------------------
# Visualise a few images
# ------------------------------------------------------------------------------
nrows = 3
ncols = 5
npics = nrows * ncols

indexes    = np.random.choice(range(len(trainset)), npics)
fig, axarr = plt.subplots(nrows, ncols)

for i, index in enumerate(indexes):
    image, label = trainset[index]
    curr_ax = axarr.ravel()[i]
    curr_ax.imshow(np.transpose(image, (1,2,0)))
    curr_ax.set_xticks([])
    curr_ax.set_yticks([])
    curr_ax.set_title(classes[label])

writer.add_figure('Sample of training data', fig)

# %%
# ------------------------------------------------------------------------------
# Defining the neural network (pre-trained)
# ------------------------------------------------------------------------------
net_tl = models.resnet18(pretrained=True)
net_tl.fc = nn.Linear(net_tl.fc.in_features, len(classes)) # .fc is the final layer!
net_tl.to(device)

# Freeze the first several layers
# https://discuss.pytorch.org/t/how-the-pytorch-freeze-network-in-some-layers-only-the-rest-of-the-training/7088/2
layers_frozen = 5
for index, child in enumerate(net_tl.children()):
    if index >= 0:
        break
    else:
        for param in child.parameters():
            param.requires_grad = False

writer.add_text('Model description: ', str(net_tl))
writer.add_graph(net_tl, trainset[0][0].unsqueeze(0).to(device))

# %%
# ------------------------------------------------------------------------------
# Defining the Loss and Optimizer
# ------------------------------------------------------------------------------

criterion = nn.CrossEntropyLoss()
#optimizer = optim.SGD(net_tl.parameters(), lr=0.01, momentum=0.8) #TUNE HYPERPARAMTERS
optimizer = optim.Adam(net_tl.parameters(), lr=8e-5, betas=(0.9, 0.999), eps=1e-8)

# %%
# ------------------------------------------------------------------------------
# Training the network (Transfer Learning)
# ------------------------------------------------------------------------------
val_metrics = {'nll': Loss(criterion),
               'accuracy': Accuracy()}

trainer   = create_supervised_trainer(net_tl, optimizer, criterion, device=device)
evaluator = create_supervised_evaluator(net_tl, metrics=val_metrics, device=device)

@trainer.on(Events.EPOCH_STARTED)
def print_time(trainer):
    print('--------------')
    print(f'Epoch[{trainer.state.epoch}] start time: {dt.datetime.now()}')

@trainer.on(Events.EPOCH_COMPLETED)
def print_train_results(trainer):
    evaluator.run(trainloader)
    metrics = evaluator.state.metrics
    epoch_no = trainer.state.epoch
    print("Training Results   - Epoch: {}  Avg loss: {:.3f} Accuracy: {:.3f}"
          .format(trainer.state.epoch, metrics["nll"], metrics['accuracy']))
    writer.add_scalar('Accuracy (Training):', metrics['accuracy'], epoch_no)
    writer.add_scalar('Cross Entropy Loss (Training):', metrics['nll'], epoch_no)

@trainer.on(Events.EPOCH_COMPLETED)
def print_test_results(trainer):
    evaluator.run(testloader)
    metrics = evaluator.state.metrics
    epoch_no = trainer.state.epoch
    print("Test Results       - Epoch: {}  Avg loss: {:.3f} Accuracy: {:.3f}"
          .format(trainer.state.epoch, metrics["nll"], metrics['accuracy']))
    writer.add_scalar('Accuracy (Test):', metrics['accuracy'], epoch_no)
    writer.add_scalar('Cross Entropy Loss (Test):', metrics['nll'], epoch_no)

trainer.run(trainloader, max_epochs=5)

#%%Setting the model to evaluation mode.

net_tl.eval()

#%% Choose the test case we want to look at with parser (ie: choose the index in our testcases):

#parser=argparse.ArgumentParser(description="Facial Recognition Parser")
#parser.add_argument('test_case_index', type=int, default=0, help='<Input the test case we want to look at>')

#parser.parse_known_args()

#Parsing arguments
#args, _ = parser.parse_known_args()
test_case_index=3 #args.test_case_index

 #This is the activation output.
actual_class          = classes[testset[test_case_index][1]] #The actual correct class of the photo.
test_case             = testset[test_case_index][0] #This is our test case for plotting the prob density.
a                     = net_tl(torch.unsqueeze(test_case.to(device),0))
softmax_func          = nn.Softmax(dim=0) #pass the activation through a softmax function.
prob_predictions      = softmax_func(softmax_func(a[0]))
prob_predictions_list = [i.item() for i in prob_predictions]

print('The actual class is:', actual_class)
print('The possible classes are:', classes)
print('The probability predictions:', prob_predictions_list)

#%% Create dataframe

my_dict = {classes[i]:[float(softmax_func(a[0])[i])] for i in range(len(classes))}
my_dict.update({'actual_class':[actual_class]})
df = pd.DataFrame(my_dict)

for i in range(1,len(testset)):
    test_case_index = i
    actual_class    = classes[testset[test_case_index][1]]
    #print(actual_class)
    test_case       = testset[test_case_index][0] #This is our test case for plotting the prob density.
    a               = net_tl(torch.unsqueeze(test_case.to(device),0))
    my_dict         = {classes[i]:[float(softmax_func(a[0])[i])] for i in range(len(classes))}
    my_dict.update({'actual_class':[actual_class]})
    new_df          = pd.DataFrame(my_dict)
    df              = pd.concat([df,new_df],axis = 0)

#df is the final dataframe
#%%

fig,ax=plt.subplots(figsize=(12,6))
df.iloc[12,:-1].plot(kind='bar',ax=ax) #chnage the index here to plot the data.
ax.set_xlabel('')
ax.set_xticklabels(classes, rotation=0)
ax.set_ylabel('Probability of Class')
ax.set_title('Probability Distribtuion of Image Classification')

# %%










