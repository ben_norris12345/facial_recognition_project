'''
Author: Ben Norris
Created: 2020-12-06

A script implementing a facial recognition tool by leveraging on neural networks.

'''
# %% file path
# ------------------------------------------------------------------------------
# File I/O
# ------------------------------------------------------------------------------
data_dir = r'C:\Users\BenNorris\Documents\Kubrick Week 12 - Deep Learning\transfer_learning_project\DeepLearning\data_faces'
tensorboard_dir=r'C:\Users\BenNorris\Documents\Kubrick Week 12 - Deep Learning\transfer_learning_project\facial_recognition_project\tensorboard_logs'
# %% imports
# ------------------------------------------------------------------------------
# Imports
# ------------------------------------------------------------------------------
import datetime as dt
import matplotlib.pyplot as plt
import numpy as np
import pandas as pd

import torch
import torch.nn as nn
import torch.optim as optim
import torchvision
import torchvision.models as models
import torchvision.transforms as transforms
from torch_lr_finder import LRFinder
from torch.utils.tensorboard import SummaryWriter

import argparse

from ignite.engine import Events, create_supervised_trainer, create_supervised_evaluator
from ignite.metrics import Loss, Accuracy
from sklearn.metrics import confusion_matrix

# %% device setup - training on the GPU
# ------------------------------------------------------------------------------
# Device Setup
# ------------------------------------------------------------------------------
device = torch.device('cuda:0' if torch.cuda.is_available() else 'cpu')
print('GPU Detected: ', torch.cuda.get_device_name())

# %%
# ------------------------------------------------------------------------------
# Tensorboard Setup
# ------------------------------------------------------------------------------

run_name = 'tensorboard_tutorial_' + dt.datetime.now().strftime('%Y-%m-%d_%H%M')
log_dir  = os.path.join(tensorboard_dir, run_name)
writer   = SummaryWriter(log_dir)

# %%
# ------------------------------------------------------------------------------
# Data Setup
# ------------------------------------------------------------------------------
IMGNET_SIZE = 224
IMGNET_MEAN = [0.485, 0.456, 0.406]
IMGNET_STD  = [0.229, 0.224, 0.225]

# setting up the transforms
transform = transforms.Compose([transforms.RandomHorizontalFlip(),
                                transforms.Resize((IMGNET_SIZE, IMGNET_SIZE)),
                                transforms.ToTensor(),
                                transforms.Normalize(IMGNET_MEAN, IMGNET_STD)])

dataset = torchvision.datasets.ImageFolder(data_dir, transform=transform)

# splitting the dataset into train and test (80% train, 20% test)
dataset_size = len(dataset)
train_size = int(dataset_size * 0.8)
test_size = dataset_size - train_size
trainset, testset = torch.utils.data.random_split(dataset, [train_size, test_size])

# creating the dataloaders
trainloader = torch.utils.data.DataLoader(trainset, batch_size=32, shuffle=True)
testloader = torch.utils.data.DataLoader(testset,  batch_size=32, shuffle=True)

# names of classes
classes = dataset.classes
print('Classes detected: ', classes)

# %% visualizing images
# ------------------------------------------------------------------------------
# Visualizations
# ------------------------------------------------------------------------------
nrows = 3
ncols = 5
npics = nrows * ncols

indexes = np.random.choice(range(len(trainset)), npics)
fig, axarr = plt.subplots(nrows, ncols)

for i, index in enumerate(indexes):
    image, label = trainset[index]
    curr_ax = axarr.ravel()[i]
    curr_ax.imshow(np.transpose(image, (1,2,0)))
    curr_ax.set_xticks([])
    curr_ax.set_yticks([])
    curr_ax.set_title(classes[label])


writer.add_figure('Sample of Training Data', fig)
# %% utilizing a pre-trained neural network (trained on images of children)
# ------------------------------------------------------------------------------
# Defining the neural network (pre-trained)
# ------------------------------------------------------------------------------
net_tl = models.resnet18(pretrained=True)
net_tl.fc = nn.Linear(net_tl.fc.in_features, len(classes)) # .fc is the final layer
net_tl.to(device) # sending the model to the device

# freeze the first several layers
'''
https://discuss.pytorch.org/t/how-the-pytorch-freeze-network-in-some-layers-only-the-rest-of-the-training/7088/2
'''

layers_frozen = 5
for index, child in enumerate(net_tl.children()):
    if index >= 0:
        break
    else:
        for param in child.parameters():
            param.requires_grad = False

writer.add_text('Model Description: ', str(net_tl))
writer.add_graph(net_tl, trainset[0][0].unsqueeze(0).to(device))

#%% defining loss and optimizer
# ------------------------------------------------------------------------------
# Defining the Loss and Optimizer
# ------------------------------------------------------------------------------
criterion = nn.CrossEntropyLoss()
optimizer = optim.Adam(net_tl.parameters(), lr=8e-5, betas=(0.9, 0.999), eps=1e-8)
 
#%% finding optimal learning rate 
# ------------------------------------------------------------------------------
# Learning Rate Finder
# ------------------------------------------------------------------------------

lr_finder = LRFinder(net_tl, optimizer, criterion, device=device)
lr_finder.range_test(trainloader, end_lr=0.05, num_iter=50)
lr_finder.plot()
lr_finder.reset()

# %% training the neural network
# ------------------------------------------------------------------------------
# Training the network (Transfer Learning)
# ------------------------------------------------------------------------------
val_metrics = {'nll': Loss(criterion),
               'accuracy': Accuracy()}

trainer = create_supervised_trainer(net_tl, optimizer, criterion, device=device)
evaluator = create_supervised_evaluator(net_tl, metrics=val_metrics, device=device)

@trainer.on(Events.EPOCH_STARTED)
def print_time(trainer):
    print('--------------')
    print(f'Epoch[{trainer.state.epoch}] start time: {dt.datetime.now()}')

@trainer.on(Events.EPOCH_COMPLETED)
def print_train_results(trainer):
    evaluator.run(trainloader)
    metrics = evaluator.state.metrics
    print("Training Results - Epoch: {}  Avg loss: {:.3f} Accuracy: {:.3f}"
          .format(trainer.state.epoch, metrics["nll"], metrics['accuracy']))

@trainer.on(Events.EPOCH_COMPLETED)
def print_test_results(trainer):
    evaluator.run(testloader)
    metrics = evaluator.state.metrics
    print("Test Results - Epoch: {}  Avg loss: {:.3f} Accuracy: {:.3f}"
          .format(trainer.state.epoch, metrics["nll"], metrics['accuracy']))

trainer.run(trainloader, max_epochs=1) 

# %% setting model to evaluation mode
# ------------------------------------------------------------------------------
# Evaluation mode
# ------------------------------------------------------------------------------

net_tl.eval()

# %% choosing test case 
# ------------------------------------------------------------------------------
# Testing the model
# ------------------------------------------------------------------------------

test_case_index = 0 

# activation output
actual_class = classes[testset[test_case_index][1]] # correct class of the photo
test_case = testset[test_case_index][0] # test case for plotting the probability density
a = net_tl(torch.unsqueeze(test_case.to(device),0))
softmax_func = nn.Softmax(dim=0) # pass the activation through a softmax function so that probabilities for each class sum to 1
prob_predictions=softmax_func(softmax_func(a[0]))
prob_predictions_list=[i.item() for i in prob_predictions]

# printing results
print('The actual class is:', actual_class)
print('The possible classes are:', classes)
print('The probability predictions:', prob_predictions_list)

# %% printing dataframe of predictions
# ------------------------------------------------------------------------------
# Predictions DataFrame 
# ------------------------------------------------------------------------------

df = pd.DataFrame({classes[0]:[float(softmax_func(a[0])[0])],
                   classes[1]:[float(softmax_func(a[0])[1])],
                   classes[2]:[float(softmax_func(a[0])[2])],
                   classes[3]:[float(softmax_func(a[0])[3])],
                   classes[4]:[float(softmax_func(a[0])[4])],
                   classes[5]:[float(softmax_func(a[0])[5])],
                   'actual_class':[actual_class]})

for i in range(1,len(testset)):
    test_case_index = i
    actual_class = classes[testset[test_case_index][1]]
    test_case = testset[test_case_index][0] # test case for plotting probability density
    a = net_tl(torch.unsqueeze(test_case.to(device),0))
    new_df = pd.DataFrame({classes[0]:[float(softmax_func(a[0])[0])],
                           classes[1]:[float(softmax_func(a[0])[1])], 
                           classes[2]:[float(softmax_func(a[0])[2])],
                           classes[3]:[float(softmax_func(a[0])[3])],
                           classes[4]:[float(softmax_func(a[0])[4])],
                           classes[5]:[float(softmax_func(a[0])[5])],
                           'actual_class':[actual_class]}) 

    df = pd.concat([df,new_df],axis=0)
    df = df.reset_index(drop=True) # final df

#%% extra
# ------------------------------------------------------------------------------
# ArgParser - This is for convenience of the user. 
# We chose sample for as a demonstration.
# ------------------------------------------------------------------------------

parser = argparse.ArgumentParser(description="Facial Recognition Parser")
parser.add_argument('plotting_index', type=float, default=1, help='<input the image you want to plot>')
parser.parse_known_args()

# parsing arguments
args, _ = parser.parse_known_args()
plotting_index=args.plotting_index

#%% Plots probability distribution
# ------------------------------------------------------------------------------
# Choosing 4th sample in test set and plotting probability distribution
# ------------------------------------------------------------------------------

plotting_index = 4
fig,ax = plt.subplots(figsize=(12,6))
df.iloc[plotting_index,:-1].plot(kind='bar',ax=ax) # change the index here to plot the data
ax.set_xlabel('')
ax.set_ylabel('Probability of Class')
ax.set_title('Probability Distribtuion of Image Classification')
ax.grid()
print('The Actual Class Is:', df.iloc[plotting_index,-1])
writer.add_text('Actual Class', str(df.iloc[plotting_index,-1]))
writer.add_figure('Probability Distribution for 4th Test Sample',fig)
#%% displays the photo of our choice
# ------------------------------------------------------------------------------
# Photo
# ------------------------------------------------------------------------------

fig, ax = plt.subplots()

image = testset[plotting_index]
ax.imshow(np.transpose(image[0], (1,2,0)))
ax.set_xticks([])
ax.set_yticks([])
writer.add_figure('The Photo of the 4th Sample In Out Testset',fig)

#%% displaying confusion matrix
# ------------------------------------------------------------------------------
# Confusion Matrix
# ------------------------------------------------------------------------------

y_true = df['actual_class']
y_pred = df.iloc[:,0:-1]
y_pred = y_pred.idxmax(axis=1)
confusion_matrix=confusion_matrix(y_true, y_pred)

print(confusion_matrix)

#%%Sending the confusion matrix to the tensorboard
fig = plt.figure(figsize=(8, 8))
plt.imshow(confusion_matrix, interpolation='nearest', cmap=plt.cm.Blues)
plt.title("Confusion matrix")
plt.colorbar()
tick_marks = np.arange(len(classes))
plt.xticks(tick_marks, classes, rotation=45)
plt.yticks(tick_marks, classes)
plt.tight_layout()
plt.ylabel('True label')
plt.xlabel('Predicted label')
writer.add_figure('Confusion Matrix For Predictions On The Testset', fig)
